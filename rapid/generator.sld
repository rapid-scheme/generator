;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;> Generators compatible with SRFI 121.

(define-library (rapid generator)
  (export generator make-iota-generator make-range-generator 
          make-coroutine-generator list->generator vector->generator
          reverse-vector->generator string->generator
          bytevector->generator
          make-for-each-generator make-unfold-generator
	  gcons* gappend gcombine gfilter gremove 
	  gtake gdrop gtake-while gdrop-while
	  gdelete gdelete-neighbor-dups gindex gselect
	  generator->list generator->reverse-list
	  generator->vector generator->vector!  generator->string
	  generator-fold generator-for-each generator-find
	  generator-count generator-any generator-every generator-unfold)
  (import (scheme base)
	  (scheme case-lambda)
	  (rapid list))
  (include "generator.scm"))
